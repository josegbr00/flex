# -*- coding: utf-8 -*-
# See LICENSE file for full copyright and licensing details.
{
    "name": "Account Voucher Payment Receipt Report",
    "version": "11.0.0.1.0",
    "license": "AGPL-3",
    "author": "Serpent Consulting Services Pvt. Ltd.",
    'maintainer': 'Serpent Consulting Services Pvt. Ltd.',
    'summary': """
    Print payment receipt
    Payment receipt report
    Print voucher
    print sales receipt
    print purchase receipt
    print customer receipt
    print supplier payment report
    payment report
    """,
    "website": "http://www.serpentcs.com",
    "description": """
    Print payment receipt
    Payment receipt report
    Print voucher
    print sales receipt
    print purchase receipt
    print customer receipt
    print supplier payment report
    payment report
""",
    'depends': ['account_voucher'],
    'data': [
             'views/account_payment_view.xml',
             'views/res_company_views.xml',
             'report/account_payment_report.xml',
             'views/report_configuration_view.xml'
             ],
    'category': 'Accounting & Finance',
    'images': ['static/description/VoucherReport.png'],
    'installable': True,
    'price': 12,
    'currency': 'EUR',
    'application': False,
    'auto_install': False,
}
