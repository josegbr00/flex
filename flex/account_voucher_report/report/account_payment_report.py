# -*- coding: utf-8 -*-
# See LICENSE file for full copyright and licensing details.
from odoo import api, models, _
from odoo.exceptions import UserError


class PaymentParser(models.AbstractModel):
    _name = 'report.account_voucher_report.report_account_payment_details'

    def get_username(self, data):
        return data.env.user.name

    @api.model
    def get_report_values(self, docids, data=None):
        docs = self.env['account.payment'].browse(docids)
        return {
            'doc_ids': docids,
            'doc_model': 'account.payment',
            'docs': docs,
            'get_username': self.get_username,
        }
