# -*- encoding: utf-8 -*-
{
	'name': 'ACCOUNT PERIOD IT',
	'category': 'account',
	'author': 'FLEXXOONE-COMPATIBLE-BO',
	'depends': ['account'],
	'version': '1.0.0',
	'description':"""

Periodo Contables
	""",
	'auto_install': False,
	'demo': [],
	'data':	['views/account_period_view.xml'],
	'installable': True
}
