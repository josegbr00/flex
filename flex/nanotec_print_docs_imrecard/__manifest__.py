# -*- coding: utf-8 -*-
{
    'name' : 'Print report docs imrecard',
    'version': '1.0',
    "author": "Flexxoone",
    'summary': 'Impresión de documentos Flexxoone',
    'category': 'Tools',
    'description':"""""",
    'depends' : ['account', 'sale'],
    'data': [
        "data/report_paperformat.xml",
        "report.xml",
        "view/report_guia_rem.xml",
    ],
    'application': True,
}
