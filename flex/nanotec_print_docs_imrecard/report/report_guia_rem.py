# -*- coding: utf-8 -*-

from odoo.tools.translate import _
import time
from openerp.osv import osv

from openerp import models, fields, api, _
from openerp.exceptions import Warning
from openerp.report import report_sxw
from . import report

class report_guia_rem(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context):
        super(report_guia_rem, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'time': time
        })


class report_saleorderqweb(osv.AbstractModel):
    _name = 'report.nanotec_print_docs_imrecard.report_guia_rem_imrecard'
    #_inherit = 'report.abstract_report'
    _template = 'nanotec_print_docs_imrecard.report_guia_rem'
    _wrapped_report_class = report_guia_rem 