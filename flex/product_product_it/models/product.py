# -*- coding: utf-8 -*-

from odoo import models, fields, api

class product_category(models.Model):

	_inherit = "product.category"
	
	codigo_categoria = fields.Char(string="Codigo")
	

class product_product(models.Model):
	_inherit = 'product.product'

	@api.model
	def name_search(self,name,args=None,operator='ilike',limit=100):
		if limit > 1 and limit < 10000:
			pass
		else:
			limit = 200
		args = args or []
		domain = []
		products =  self.search([],limit=limit)
		if name:
			namex = name.split(' ')
			do = []
			for i in namex:
				do.append( ('name','ilike',i) )

			products = self.search(do,limit=limit)
		if not products:
			domain = [ ('default_code','=',name) ]
			products = self.search(domain,limit=limit)
		
		return products.name_get()


class product_template(models.Model):
	_inherit = 'product.template'

	analytic_account_id = fields.Many2one('account.analytic.account','Cuenta Analitica')

	@api.model
	def name_search(self,name,args=None,operator='ilike',limit=100):
		if limit > 1 and limit < 10000:
			pass
		else:
			limit = 200
		args = args or []
		domain = []
		products =  self.search([],limit=limit)
		if name:
			namex = name.split(' ')
			do = []
			for i in namex:
				do.append( ('name','ilike',i) )

			products = self.search(do,limit=limit)
		if not products:
			domain = [ ('default_code','=',name) ]
			products = self.search(domain,limit=limit)
		
		return products.name_get()