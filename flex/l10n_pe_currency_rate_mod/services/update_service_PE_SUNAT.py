# -*- coding: utf-8 -*-
# © 2009 Camptocamp
# © 2013-2014 Agustin Cruz openpyme.mx
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).
from odoo.exceptions import UserError
from .currency_getter_interface import CurrencyGetterInterface
from odoo import fields
from datetime import datetime,timedelta
import pytz

import logging
_logger = logging.getLogger(__name__)


class PeSunatGetter(CurrencyGetterInterface):
    """Implementation of Currency_getter_factory interface
    for SUNAT service

    """
    code = 'PE_SUNAT'
    name = 'Superintendencia Nacional de Aduanas y de Administración Tributaria'
    supported_currency_array = [
        "PEN", "USD"]
        
    def get_updated_currency(self, currency_array, main_currency,
                             max_delta_days=1, date = None):
        """implementation of abstract method of Curreny_getter_interface"""
        logger = logging.getLogger(__name__)
        # we do not want to update the main currency
        if main_currency in currency_array:
            currency_array.remove(main_currency)

        # Suported currencies
        suported = ['USD']
        rates = self.rate_retrieve(date)
        for curr in currency_array:
            if curr in suported:
                rate = rates.get(curr)
                if rate:
                    rate['purchase_value'] = rates.get(curr).get('purchase_value') and 1/rates.get(curr).get('purchase_value') or 0
                    rate['sale_value'] =  rates.get(curr).get('sale_value') and 1/rates.get(curr).get('sale_value') or 0
                    self.updated_currency[curr] =rates.get(curr)
            else:
                # No other currency supported
                continue
            logger.debug("Rate retrieved : %s = %s %s" %
                         (main_currency, str(rates), curr))
        return self.updated_currency, self.log_info

    def rate_retrieve(self, fecha_consulta=None):
        from datetime import datetime, timedelta
        from urllib.request import urlopen
        from urllib.parse import urlencode

        logger = logging.getLogger(__name__)
        logger.debug("SUNAT currency rate service : connecting...")

        fecha_inicial = datetime.now()

        if not fecha_consulta:
            #dias = timedelta(days=4)
            today = datetime.now() #- dias
            tz_name = "America/Lima"
            today_utc = pytz.timezone('UTC').localize(today, is_dst=False)
            fecha_inicial = today_utc.astimezone(pytz.timezone(tz_name))
        else:
            fecha_inicial = datetime.strptime(fecha_consulta, '%Y-%m-%d')

        str_fecha_inicial = fields.Date.to_string(fecha_inicial)
        mesini = fecha_inicial.month - 1
        aoini = fecha_inicial.year
        if mesini == 0:
            mesini = 12
            aoini += -1
        mes_ini = datetime(year=aoini, month=mesini, day=1)
        mes_fin = datetime(year=fecha_inicial.year, month=fecha_inicial.month, day=1)
        rango_mes = []

        while mes_ini != mes_fin:
            rango_mes.append([str(mes_ini.month), str(mes_ini.year)])
            mes_ini = datetime(year=mes_ini.year + int((mes_ini.month + 1) / 13), month=(mes_ini.month % 12) + 1,
                               day=1)

        rango_mes.append([str(mes_ini.month), str(mes_ini.year)])

        saldos = []
        dato_sal = [0, 0, 0]
        cont = 0
        diaanterior = None
        for meses in rango_mes:
            mes = meses[0]
            anho = meses[1]
            datos = urlencode({'mes': mes, 'anho': anho})
            url = "http://www.sunat.gob.pe/cl-at-ittipcam/tcS01Alias?"
            try:
                res = urlopen(url + datos)
            except:
                raise UserError('Error: No se puede conectar a la página de Sunat!')
            from bs4 import BeautifulSoup
            soup = BeautifulSoup(res.read())
            table = soup.findAll('table')[1]

            for i in table.findAll("tr")[1:]:
                for j in i.findAll("td"):
                    dato_sal[cont] = j.text
                    if cont == 2:
                        dia_actual = datetime(year=int(anho), month=int(mes), day=int(dato_sal[0]))
                        while (diaanterior and diaanterior + timedelta(days=1) != dia_actual):
                            diaanterior = diaanterior + timedelta(days=1)
                            saldos.append(
                                [fields.Date.to_string(diaanterior), diaanterior.day, saldos[-1][2], saldos[-1][3]])
                        saldos.append([fields.Date.to_string(dia_actual), dato_sal[0], dato_sal[1], dato_sal[2]])
                        diaanterior = dia_actual
                    cont = (cont + 1) % 3

        dic_sal = {}
        for i in saldos:
            dic_sal[i[0]] = [i[2], i[3]]

        data = {}

        if dic_sal.get(str_fecha_inicial):
            resultado = dic_sal.get(str_fecha_inicial)
            data = {
                "date": str_fecha_inicial,
                "rates": [
                    {
                        "name": "USD",
                        "currency_code": "02",
                        "description": "Dolar de N.A.",
                        "purchase_value": float(resultado[0].strip()),
                        "sale_value": float(resultado[1].strip()),
                    },
                ],
                'date_consultation': fecha_inicial.isoformat()
            }
        else:
            data = {
                "detail": "Not found."
            }
            raise UserError('Error: No existe tipo de cambio para la fecha ' + str_fecha_inicial)


        logger.debug("data:", data)

        if data.get('rates'):
            logger.debug("SUNAT sent a valid json file")
        else:
            purchase = False
            sale = False
        rates = {}
        for rate in data.get('rates', []):
            rates[rate.get('name', '')] = rate
        return rates