# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import UserError

# def do_auto(self):
from urllib.request import urlopen
from urllib.parse import urlencode
from datetime import timedelta
import pytz


def rate_retrieve(self, fecha_consulta=None):
    from datetime import datetime, timedelta

    fecha_inicial = datetime.now()

    if not fecha_consulta:
        dias = timedelta(days=3)
        today = datetime.now() - dias
        tz_name = "America/Lima"
        today_utc = pytz.timezone('UTC').localize(today, is_dst=False)
        fecha_inicial = today_utc.astimezone(pytz.timezone(tz_name))

    str_fecha_inicial = fields.Date.to_string(fecha_inicial)
    mesini = fecha_inicial.month - 1
    aoini = fecha_inicial.year
    if mesini == 0:
        mesini = 12
        aoini += -1
    mes_ini = datetime(year=aoini, month=mesini, day=1)
    mes_fin = datetime(year=fecha_inicial.year, month=fecha_inicial.month, day=1)
    rango_mes = []

    while mes_ini != mes_fin:
        rango_mes.append([str(mes_ini.month), str(mes_ini.year)])
        mes_ini = datetime(year=mes_ini.year + int((mes_ini.month + 1) / 13), month=(mes_ini.month % 12) + 1,
                           day=1)

    rango_mes.append([str(mes_ini.month), str(mes_ini.year)])

    saldos = []
    dato_sal = [0, 0, 0]
    cont = 0
    diaanterior = None
    for meses in rango_mes:
        mes = meses[0]
        anho = meses[1]
        datos = urlencode({'mes': mes, 'anho': anho})
        url = "http://www.sunat.gob.pe/cl-at-ittipcam/tcS01Alias?"
        try:
            res = urlopen(url + datos)
        except:
            raise UserError('Error!', 'No se puede conectar a la página de Sunat!')
        from bs4 import BeautifulSoup
        soup = BeautifulSoup(res.read())
        table = soup.findAll('table')[1]

        for i in table.findAll("tr")[1:]:
            for j in i.findAll("td"):
                dato_sal[cont] = j.text
                if cont == 2:
                    dia_actual = datetime(year=int(anho), month=int(mes), day=int(dato_sal[0]))
                    while (diaanterior and diaanterior + timedelta(days=1) != dia_actual):
                        diaanterior = diaanterior + timedelta(days=1)
                        saldos.append([fields.Date.to_string(diaanterior), diaanterior.day, saldos[-1][2], saldos[-1][3]])
                    saldos.append([fields.Date.to_string(dia_actual), dato_sal[0], dato_sal[1], dato_sal[2]])
                    diaanterior = dia_actual
                cont = (cont + 1) % 3

    dic_sal = {}
    for i in saldos:
        dic_sal[i[0]] = [i[2], i[3]]

    final = {}

    if dic_sal.get(str_fecha_inicial):
        resultado = dic_sal.get(str_fecha_inicial)
        final = {
            "date": str_fecha_inicial,
            "rates": [
                {
                    "name": "USD",
                    "currency_code": "02",
                    "description": "Dolar de N.A.",
                    "purchase_value": resultado[0].strip(),
                    "sale_value": resultado[1].strip(),
                },
            ],
            'date_consultation': fecha_inicial.isoformat()
        }
    else:
        final = {
            "detail": "Not found."
        }


rate_retrieve(None)
