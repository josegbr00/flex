# -*- coding: utf-8 -*-
# © 2008-2016 Camptocamp
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).
{
    "name": "Peruvian Currency Rate Update - from SUNAT ",
    "version": "0.1",
    "author": "Camptocamp,Odoo Community Association (OCA), Grupo YACCK, flexxoone",
    "website": "http://flexxoone.com/",
    "license": "AGPL-3",
    "category": "Financial Management/Configuration",
    "depends": [
        "base",
        "account",  # Added to ensure account security groups are present
    ],
    "data": [
        "data/cron.xml",
        "wizard/currency_rate_update_wizard_view.xml",
        "views/currency_rate_update.xml",
        "views/account_config_settings.xml",
        "views/res_currency_view.xml",
        "security/rule.xml",
        "security/ir.model.access.csv",
    ],
    'installable': True
}
