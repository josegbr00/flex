# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import UserError

class account_journal(models.Model):
    _inherit = 'account.journal'

    account_line_ids = fields.One2many("account.journal.line", "journal_id", "Account Mirrors")
    is_mirror = fields.Boolean("Is Mirror")

class account_journal_line(models.Model):
    _name = 'account.journal.line'

    name = fields.Char("Name")
    src_account_id = fields.Many2one("account.account", "Source Account")
    dst_account_id = fields.Many2one("account.account", "Destination Account")
    journal_id = fields.Many2one("account.journal", "Journal")

class account_move(models.Model):
    _inherit = 'account.move'

    @api.multi
    def post(self):
        for move_id in self:
            if move_id.journal_id.is_mirror and self.env.context:
                for line in move_id.line_ids:
                    mirror_account_id = self.env['account.journal.line'].search([('journal_id', '=', self.journal_id.id), 
                                                                                 ('src_account_id', '=',line.account_id.id)],
                                                                                limit=1)
                    if mirror_account_id:
                        dst_line_id = line.with_context(exclude_assert_balanced=True).copy()
                        dst_line_id.account_id = mirror_account_id.dst_account_id.id
                    else:
                        raise UserError( _('You cannot create journal items on a mirror account not defined %s %s.') % (line.account_id.code, line.account_id.name))
        return super(account_move, self).post()

    
#     @api.multi
#     def button_validate(self):
#         self.ensure_one()
#         if self.journal_id.is_mirror:
#             for line in self.line_id:
#                 mirror_account_id = self.env['account.journal.line'].search([('journal_id', '=', self.journal_id.id), 
#                                                                              ('src_account_id', '=',line.account_id.id)],
#                                                                             limit=1)
#                 if mirror_account_id:
#                     dst_line_id = line.copy()
#                     dst_line_id.account_id = mirror_account_id.dst_account_id.id
#                 else:
#                     raise except_orm(_('Error!'), _('You cannot create journal items on a mirror account not defined %s %s.') % (line.account_id.code, line.account_id.name))
#         return super(account_move, self).button_validate()

#class account_move_line(models.Model):
#    _inherit = 'account.move.line'
    
    