# -*- coding: utf-8 -*-
{
    'name': "account_mirror",

    'summary': """
        account_mirror""",

    'description': """
        account_mirror
    """,

    'author': "Cubic ERP",
    'website': "http://www.cubicERP.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Financial',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': [
        'account',
    ],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/account_view.xml',
    ],
}