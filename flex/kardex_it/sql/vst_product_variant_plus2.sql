-- View: public.vst_product_variant_plus2

-- DROP VIEW public.vst_product_variant_plus2;

CREATE OR REPLACE VIEW public.vst_product_variant_plus2 AS
 SELECT product_product.id,
    count(*) AS count
   FROM product_product
     LEFT JOIN product_attribute_value_product_product_rel ON product_product.id = product_attribute_value_product_product_rel.product_product_id
     LEFT JOIN product_attribute_value ON product_attribute_value_product_product_rel.product_attribute_value_id = product_attribute_value.id
     LEFT JOIN product_attribute ON product_attribute_value.attribute_id = product_attribute.id
  WHERE product_product.active = true
  GROUP BY product_product.id
 HAVING count(*) > 1
  ORDER BY (count(*));