-- View: public.vst_product_all

-- DROP VIEW public.vst_product_all;

CREATE OR REPLACE VIEW public.vst_product_all AS
 SELECT product_product.id,
    product_product.default_code AS codigo,
    product_template.name AS producto,
    product_category.name AS categoria,
    product_brand.name AS marca,
    product_uom.name AS unidad
   FROM product_product
     JOIN product_template ON product_product.product_tmpl_id = product_template.id
     JOIN product_category ON product_template.categ_id = product_category.id
     LEFT JOIN product_brand ON product_template.product_brand_id = product_brand.id
     JOIN product_uom ON product_template.uom_id = product_uom.id
  WHERE product_product.active = true;
