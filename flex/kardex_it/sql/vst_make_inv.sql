-- View: public.vst_make_inv
 
-- DROP VIEW public.vst_make_inv;

CREATE OR REPLACE VIEW public.vst_make_inv AS
 SELECT vst_product_all.id,
    vst_product_all.codigo,
    vst_product_all.producto,
    vst_product_all.categoria,
    vst_product_all.marca,
    vst_product_all.unidad,
    product_attribute.name AS atributo,
    product_attribute_value.name AS valor
   FROM vst_product_all
     JOIN product_attribute_value_product_product_rel ON vst_product_all.id = product_attribute_value_product_product_rel.product_product_id
     JOIN product_attribute_value ON product_attribute_value_product_product_rel.product_attribute_value_id = product_attribute_value.id
     JOIN product_attribute ON product_attribute_value.attribute_id = product_attribute.id
  WHERE (vst_product_all.id IN ( SELECT vst_product_variant_plus2.id
           FROM vst_product_variant_plus2))
UNION ALL
 SELECT vst_product_all.id,
    vst_product_all.codigo,
    vst_product_all.producto,
    vst_product_all.categoria,
    vst_product_all.marca,
    vst_product_all.unidad,
    product_attribute.name AS atributo,
    product_attribute_value.name AS valor
   FROM vst_product_all
     LEFT JOIN product_attribute_value_product_product_rel ON vst_product_all.id = product_attribute_value_product_product_rel.product_product_id
     LEFT JOIN product_attribute_value ON product_attribute_value_product_product_rel.product_attribute_value_id = product_attribute_value.id
     LEFT JOIN product_attribute ON product_attribute_value.attribute_id = product_attribute.id
  WHERE NOT (vst_product_all.id IN ( SELECT vst_product_variant_plus2.id
           FROM vst_product_variant_plus2));

