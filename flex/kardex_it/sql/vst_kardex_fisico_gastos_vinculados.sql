-- View: public.vst_kardex_fisico_gastos_vinculados

-- DROP VIEW public.vst_kardex_fisico_gastos_vinculados;

CREATE OR REPLACE VIEW public.vst_kardex_fisico_gastos_vinculados AS
 SELECT COALESCE(pt.unidad_kardex, pt.uom_id) AS product_uom,
    NULL::integer AS move_dest_id,
    aml.debit - aml.credit AS price_unit,
    0 AS product_qty,
    NULL::integer AS location_id,
    sl.id AS location_dest_id,
    NULL::integer AS picking_type_id,
    pp.id AS product_id,
    NULL::integer AS picking_id,
    aml.id AS invoice_id,
    am.fecha_contable AS date,
    aml.nro_comprobante AS name,
    am.partner_id,
    '00'::character varying(4) AS guia,
    NULL::integer AS analitic_id,
    NULL::integer AS id,
    pp.default_code,
    'done'::character varying AS estado
   FROM account_move am
     LEFT JOIN account_move_line aml ON aml.move_id = am.id
     JOIN product_product pp ON pp.id = aml.product_id
     JOIN product_template pt ON pt.id = pp.product_tmpl_id
     JOIN stock_location sl ON sl.id = aml.location_id
     LEFT JOIN account_analytic_tag_account_move_line_rel aataml ON aataml.account_move_line_id = aml.id
     CROSS JOIN main_parameter mp
  WHERE mp.etiqueta_analitica = aataml.account_analytic_tag_id
UNION ALL
 SELECT COALESCE(pt.unidad_kardex, pt.uom_id) AS product_uom,
    NULL::integer AS move_dest_id,
    gvdd.monto AS price_unit,
    0 AS product_qty,
    NULL::integer AS location_id,
    sl.id AS location_dest_id,
    sp.picking_type_id,
    pp.id AS product_id,
    NULL::integer AS picking_id,
    gvd.id AS invoice_id,
    gvd.fecha AS date,
    gvdd.nro_comprobante AS name,
    gvd.proveedor AS partner_id,
    '00'::character varying(4) AS guia,
    NULL::integer AS analitic_id,
    NULL::integer AS id,
    pp.default_code,
    'done'::character varying AS estado
   FROM gastos_vinculados_distribucion gvd
     JOIN gastos_vinculados_distribucion_detalle gvdd ON gvdd.distribucion_id = gvd.id
     JOIN stock_move sm ON gvdd.move_id = sm.id
     JOIN stock_picking sp ON sp.id = sm.picking_id
     JOIN product_product pp ON pp.id = sm.product_id
     JOIN product_template pt ON pt.id = pp.product_tmpl_id
     JOIN stock_location sl ON sl.id = sp.location_dest_id
  WHERE gvd.state::text = 'done'::text;


