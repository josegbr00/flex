# -*- coding: utf-8 -*-

from odoo import api, models, fields, _

class picking_ids(models.Model):

    _inherit = 'account.invoice'

    picking_ids = fields.One2many('stock.picking', compute='_get_picking_ids', store=False, string='Invoice associated to picking')

    @api.one
    def _get_picking_ids(self):
        self.picking_ids = self.env["stock.picking"].search([("origin", "=", self.origin)]) 
