# -*- encoding: utf-8 -*-
{
	'name': 'Account Move Advance Add IT',
	'category': 'account',
	'author': 'FLEXXOONE-COMPATIBLE-BO',
	'depends': ['import_base_it'],
	'version': '1.0',
	'description':"""
	Agrega lineas a los asientos contables con un wizard dinamico
	Imprime Asientos Contables en Excel
	""",
	'auto_install': False,
	'demo': [],
	'data':	['views/account_move_view.xml'],
	'installable': True
}
