# -*- coding: utf-8 -*-
{
    'name': 'Sale order approve discount',
    'version': '',
    'category': 'Sales Management',
    'author': 'Flexxoone SAC',
    'website': 'www.flexxoone.com',
    'license': 'AGPL-3',
    'summary': 'Este módulo permite validar los pedidos de venta solo a personal autorizado',
    'depends': [
        'sale',
        #'fxo_sale_three_discounts',
        'website_quote',
    ],
    'data': [
        'views/sale_order_view.xml',
        'security/view_approve_order_menu.xml',
        'security/ir.model.access.csv',
        "views/sale_order_approval.xml",
    ],
    'installable': True,
}
