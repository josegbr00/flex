# -*- coding: utf-8 -*-
from odoo import fields, models, api
from ast import literal_eval
import pudb


class SaleOrder(models.Model):
    _inherit = "sale.order"

    approval_line = fields.One2many('sale.order.approval', 'approval_id', string='Motivos', )

    @api.one
    @api.depends('order_line')
    def _compute_approv_group_permission(self):

        exist_discount2 = False
        require_approval = literal_eval(self.env["ir.config_parameter"].get_param('fxo_sale_three_discounts.validate_discounts', 'False'))
        for item in self.order_line:
            if float(item.discount2) > 0.00:
                exist_discount2 = True
                break

        #Agregando validacion para el descuento1
        exist_discount1 = False
        require_approval1 = literal_eval(self.env["ir.config_parameter"].get_param('fxo_sale_three_discounts.validate_discounts1', 'False'))
        for item in self.order_line:
            if float(item.discount1) > 0.00:
                exist_discount1 = True
                break

        #Agregando condicion (Si esta marcado la opcion de la configuracion y hay decsuento requiere aprobacion)
        if (require_approval and exist_discount2) or (require_approval1 and exist_discount1):
            self.hide_buttons_order = True

    @api.one
    def approve_order(self):
        """El Usuario Solicitando Aprobacion"""

        require_approval = False
        for line in self.order_line:
            if line.require_approval:
                require_approval = True

                move_line_1 = {
                    'reason': 'El producto: ' + line.product_id.name + ', tiene descuento extra del ' + str(line.discount2) + u' %, requiere aprobación !.',
                    'date': fields.Date.context_today(self),
                    'state': 'detained',
                }
                move_vals = {
                    'approval_line': [(0, 0, move_line_1)],
                }
                self.write(move_vals)

        #Actualiza el sale.order
        self.write({'state': 'forapprove', 'require_approval': require_approval})

        #self.write({'state': 'forapprove'})

    @api.one
    def approve_sale_order(self):
        """El admin Aprobando la orden"""

        #pudb.set_trace()
        # move_line_1 = {
        #     'reason': 'Se aprueba el(los) descuento(s) adicional(es) (descuento 2) solicitado en el detalle de la Cotización.',
        #     'date': fields.Date.context_today(self),
        #     'state': 'approved',
        # }
        #self.write({'state': 'approved', 'require_approval': True, 'approval_line': [(0, 0, move_line_1)]})

        # self.write({'state': 'approved'})
        pass

    state = fields.Selection([
        ('draft', 'Quotation'),
        ('forapprove', 'Por aprobar'),
        ('approved', 'Aprobado'),
        ('sent', 'Quotation Sent'),
        ('sale', 'Sale Order'),
        ('done', 'Done'),
        ('cancel', 'Cancelled'),
    ], string='Status', readonly=True, copy=False, index=True, track_visibility='onchange', default='draft')

    hide_buttons_order = fields.Boolean(compute=_compute_approv_group_permission)

    @api.model
    def create(self, vals):
        if vals.get('name', 'New') == 'New':
            vals['name'] = self.env['ir.sequence'].next_by_code('sale.order') or 'New'

        if any(f not in vals for f in ['partner_invoice_id', 'partner_shipping_id', 'pricelist_id']):
            partner = self.env['res.partner'].browse(vals.get('partner_id'))
            addr = partner.address_get(['delivery', 'invoice'])
            vals['partner_invoice_id'] = vals.setdefault('partner_invoice_id', addr['invoice'])
            vals['partner_shipping_id'] = vals.setdefault('partner_shipping_id', addr['delivery'])
            vals['pricelist_id'] = vals.setdefault('pricelist_id',
                                                   partner.property_product_pricelist and partner.property_product_pricelist.id)
        result = super(SaleOrder, self).create(vals)
        return result
