from odoo import fields, models, api
from odoo.tools.translate import _
#import pudb


class RequireApproval(models.Model):

    _name = "sale.order.approval"

    name = fields.Char(string='Nombre', copy=False)
    reason = fields.Char(string='Motivo', copy=False)
    date = fields.Date(string='Fecha', copy=False)
    state = fields.Selection([
        ('detained', 'Retenido'),
        ('refused', 'Rechazado'),
        ('approved', 'Aprobado'),
    ], string='Estado', readonly=True, copy=False, index=True)
    user_id = fields.Many2one('res.users', string='Usuario', index=True, track_visibility='onchange', default=lambda self: self.env.user)
    approval_id = fields.Many2one('sale.order', string='Order Reference', required=False, ondelete='cascade', index=True, copy=False)
    notes = fields.Text('Notes')

    @api.one
    def act_confirm(self):
        #pudb.set_trace()
        self.approval_id.state="approved"
        self.write({'state': 'approved'})

    @api.one
    def state_refused(self):
        self.write({'state': 'refused'})

    @api.one
    def state_cancel(self):
        self.write({'state': 'forapprove'})
