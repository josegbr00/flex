# -*- coding: utf-8 -*-
# Copyright YEAR(S), AUTHOR(S)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import models, api


class ReportProductBarcode(models.AbstractModel):
    _name = 'report.bo_print_barcode.product_barcode_template'

    @api.model
    def get_report_values(self, docids, data=None):
        records = []
        if data.get('product_ids'):
            records = self.env["product.product"].browse(data.get('product_ids'))
        return {
            'docs': records,
        }
