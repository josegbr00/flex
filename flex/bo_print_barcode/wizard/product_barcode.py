# -*- coding: utf-8 -*-

from odoo import fields, models, api


class ProductBarcode(models.TransientModel):
    _name = 'product.barcode.wizard'

    line_ids = fields.One2many(
        string=u'Líneas de productos',
        comodel_name='product.barcode.line.wizard',
        inverse_name='product_barcode_id'
    )

    @api.model
    def default_get(self, fields):
        record_ids = self._context.get('active_ids')
        result = super(ProductBarcode, self).default_get(fields)
        line_values = []
        if record_ids:
            if 'line_ids' in fields:
                for prod in self.env['product.product'].browse(record_ids):
                    line_values.append((0, 0, {
                        'product_id': prod.id,
                        'quantity': 1,
                        
                    }))
            result['line_ids'] = line_values
        return result

    @api.multi
    def generate_report(self):
        list_ids = []
        for line in self.line_ids:
            qty = line.quantity
            while qty > 0:
                list_ids.append(line.product_id.id)
                qty -= 1

        data = {
            
            "product_ids": list_ids
        }
        return self.env.ref('bo_print_barcode.report_product_barcode').report_action([], data=data)


class ProductBarcodeLine(models.TransientModel):
    _name = 'product.barcode.line.wizard'

    product_barcode_id = fields.Many2one(
        comodel_name='product.barcode.wizard',
        string="Referencia"
    )
    product_id = fields.Many2one(
        comodel_name='product.product',
        string="Producto",
    )
    quantity = fields.Integer(
        string="Cantidad",
        help=u"Indica la cantidad de veces que imprimirá"
        u" el código de barras del producto"
    )
    
