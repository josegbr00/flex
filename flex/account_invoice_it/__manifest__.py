# -*- encoding: utf-8 -*-
{
	'name': 'ACCOUNT INVOICE IT',
	'category': 'account',
	'author': 'FLEXXOONE-COMPATIBLE-BO',
	'depends': ['account','odoope_einvoice_base','account_invoice_series_it'],
	'version': '2.0.0',
	'description':"""
	CAMPOS DEL ACCOUNT INVOICE
	""",
	'auto_install': False,
	'demo': [],
	'data':	['views/invoice_it_view.xml'],
	'installable': True
}
