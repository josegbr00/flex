# -*- encoding: utf-8 -*-
{
	'name': 'Kardex Producto IT',
	'category': 'account',
	'author': 'FLEXXOONE-COMPATIBLE-BO',
	'depends': ['import_base_it'],
	'version': '1.0',
	'description':"""
	Kardex Producto IT, genera el kardex desde producto
	""",
	'auto_install': False,
	'demo': [],
    'qweb' : [],
	'data':	['views/product_template_view.xml'],
	'installable': True
}
