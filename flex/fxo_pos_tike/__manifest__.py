# -*- coding: utf-8 -*-
{
    'name': "Peruvian POS TIKE",

    'summary': """
        Peruvian Management POS""",

    'description': """
        Peruvian Management POS add filds descripton , quantity and unit_price  in pos tike
    """,

    'author': "FLEXXOONE",
    'website': "http://www.flexxoone.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Localization/Peruvian',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': [
        'point_of_sale',
    ],

    # always loaded
    'data': [
        # 'views/template.xml',
    ],
    'qweb': [
        'static/src/xml/pos.xml'
    ],
}