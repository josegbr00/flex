# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

import json
import logging
from werkzeug.exceptions import Forbidden

from odoo import http, SUPERUSER_ID, tools, _
from odoo.http import request
from odoo.addons.base.ir.ir_qweb.fields import nl2br


    
class WebsiteModalPopup(http.Controller):


    @http.route('/send/email', type="http", auth="public", website=True)
    def send_email(self, **post):
        cr, uid, context, registry = request.cr, request.uid, request.context, request.registry
        email = request.env['res.users'].browse(uid).company_id.email
        name = post['name']
        email_to = email
        email_from = post['email']
        
        vals = {
                'subject':name,
                'email_from':email_from,
                'email_to' : email_to,
                }
        
        vals['body_html'] = "<p>Inquiry Generated</p>" + "<br/> Name :  " + post['name']  + "<br/> City :  " + post['city'] + "<br/> Phone : " + post['phone'] + "<br/> Email :  " + post['email']
        mail_mail_obj = request.env['mail.mail']
        msg_id = mail_mail_obj.create(vals)
        if msg_id:
            res = mail_mail_obj.send(msg_id.id)
                                                                       
        return request.render("website_popup_modal.email_thank_you")

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:        
