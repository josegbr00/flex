# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

{
    "name" : "Website Popups With Inquiry",
    "version" : "11.0.0.2",
    "category" : "eCommerce",
    "depends" : ['base','website','website_sale'],
    "author": "Flexxoone",
    'summary': 'Website Pop up window which send email to company with details.',
    "description": """
    
    Purpose :- 
    This Module Helps Pop up wizard which send email to company with details.
    Website pop-up window open when leaving website, Website pop-up window open when leaving webshop
    website popups window, inquiry from popup, website inquiry, website popup inquire, website popup registration, website registration, website popup model
        website pop-ups window, inquiry from pop-up, website inquiry, website pop-up inquire, website pop-up registration, website registration, website pop-up model
        
    """,
    "website" : "www.flexxoone.com",
    "data": [
        'views/templates.xml',
    ],
    'price': '10.00',
    'currency': "PEN",
    "auto_install": False,
    "application": True,
    "installable": True,
    'images':['static/description/Banner.png'],
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
