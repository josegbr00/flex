# -*- encoding: utf-8 -*-
{
	'name': 'Account Parametros IT',
	'category': 'account',
	'author': 'FLEXXOONE-COMPATIBLE-BO',
	'depends': ['account','odoope_einvoice_base','account_tax_code_it'],
	'version': '1.0.0',
	'description':"""
	Parametros Generales para el Sistema de Odoo
	""",
	'auto_install': False,
	'demo': [],
	'data':	['views/main_parameter_view.xml'],
	'installable': True
}
