# -*- coding: utf-8 -*-
{
    'name': 'Account Invoice Excel Report',
    'summary': "Export Account Invoice Excel Report",
    'description': """Export Account Invoice Excel Report""",

    'author': 'iPredict IT Solutions Pvt. Ltd.',
    'website': 'http://ipredictitsolutions.com',
    "support": "ipredictitsolutions@gmail.com",

    'category': 'Reports',
    'version': '11.0.0.1.1',
    'depends': ['account_invoicing', 'l10n_pe_vat'],
    'data': [
        'security/ir.model.access.csv',
        'wizard/account_invoice_excel_report.xml',
        #'wizard/account_invoice_excel_report_bill.xml',
        'views/invoice_excel_report_menu_view.xml',
    ],

    'license': "OPL-1",

    'auto_install': False,
    'installable': True,

    'images': ['static/description/main.png'],
}
