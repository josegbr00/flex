# -*- coding: utf-8 -*-

import base64
from io import BytesIO
import datetime
import unicodedata
import xlwt
from xlwt import Workbook

from odoo import models, fields, api, _
from odoo.exceptions import UserError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT

class AccountInvoiceExcelReport(models.TransientModel):
    _name = 'account.invoice.excel.report'

    start_date = fields.Date('Start Date', required=True)
    end_date = fields.Date('End Date', required=True)

    @api.multi
    def account_invoice_excel_report(self):
        dt_start = datetime.datetime.strptime(self.start_date, DEFAULT_SERVER_DATE_FORMAT)
        date_start = dt_start.strftime('%d-%m-%Y')
        dt_end = datetime.datetime.strptime(self.end_date, DEFAULT_SERVER_DATE_FORMAT)
        date_end = dt_end.strftime('%d-%m-%Y')
        filename = 'Invoice Report From ' + str(date_start) + ' To ' + str(date_end) + '.xls'
        workbook = xlwt.Workbook(encoding="UTF-8")

        worksheet = workbook.add_sheet('Account Invoice Details')
        font = xlwt.Font()
        font.bold = True

        GREEN_TABLE_HEADER = xlwt.easyxf(
                 'font: bold 1, name Tahoma, height 250;'
                 'align: vertical center, wrap on;'
                 #'borders: top medium, bottom medium, left medium, right medium;'
                 )
        style = xlwt.easyxf('font:height 400, bold True, name Arial; align: horiz center, vert center;borders: top medium,right medium,bottom medium,left medium')
        #for_left_center1 = xlwt.easyxf("font: bold 1, color black; align: horiz center")
        #for_left_center2 = xlwt.easyxf("font: color black; align: horiz center")
        for_left_center1 = xlwt.easyxf("font: bold 1, color black; align: horiz center; borders: top medium, bottom medium, left medium, right medium;pattern: pattern solid, pattern_fore_colour lime, pattern_back_colour lime;")
        for_left_center2 = xlwt.easyxf("font: color black; align: horiz left; borders: top medium, bottom medium, left medium, right medium;")
        for_left_center3 = xlwt.easyxf("font: color black; align: horiz right; borders: top medium, bottom medium, left medium, right medium;")
        for_left_center4 = xlwt.easyxf("font: color black; align: horiz center; borders: top medium, bottom medium, left medium, right medium;")

        alignment = xlwt.Alignment()
        alignment.horz = xlwt.Alignment.HORZ_RIGHT
        style = xlwt.easyxf('align: wrap yes')
        style.num_format_str = '0.00'

        worksheet.row(0).height = 320
        worksheet.col(0).width = 4000
        worksheet.col(1).width = 4000
        worksheet.col(2).width = 4000
        worksheet.col(3).width = 3000
        worksheet.col(4).width = 3000
        worksheet.col(5).width = 4000
        worksheet.col(6).width = 2000
        worksheet.col(7).width = 4000
        worksheet.col(8).width = 7000
        worksheet.col(9).width = 4000
        worksheet.col(10).width = 4000
        worksheet.col(11).width = 4000
        worksheet.col(12).width = 4000
        worksheet.col(13).width = 4000
        worksheet.col(14).width = 4000
        worksheet.col(15).width = 4000
        worksheet.col(16).width = 3000
        worksheet.col(17).width = 3000
        worksheet.col(18).width = 4000
        worksheet.col(19).width = 4000        
        worksheet.col(20).width = 4000        
        worksheet.col(21).width = 4000        
        worksheet.col(22).width = 4000        

        borders = xlwt.Borders()
        borders.bottom = xlwt.Borders.MEDIUM
        border_style = xlwt.XFStyle()
        border_style.borders = borders

        #invoice_details_title = _('Customer Invoices')
        #worksheet.write_merge(0, 1, 0, 13, invoice_details_title, GREEN_TABLE_HEADER)

        company = self.env['res.company'].search([])[0].partner_id          
        invoice_details_title = _('Customer Invoices')
        worksheet.write_merge(0, 0, 0, 18, invoice_details_title, GREEN_TABLE_HEADER)
        worksheet.write_merge(1, 1, 0, 0, "Periodo:".upper(), GREEN_TABLE_HEADER)    
        worksheet.write_merge(1, 1, 1, 1, self.start_date)
        worksheet.write_merge(1, 1, 2, 2, self.end_date)
        worksheet.write_merge(2, 2, 0, 0, "RUC:", GREEN_TABLE_HEADER)
        worksheet.write_merge(2, 2, 1, 1, company.company_id.partner_id.doc_number)
        worksheet.write_merge(3, 3, 0, 0, "Empresa:".upper(), GREEN_TABLE_HEADER)
        worksheet.write_merge(3, 3, 1, 1, company.name.upper(), )
        worksheet.write_merge(4, 4, 0, 0, u"Dirección:".upper(), GREEN_TABLE_HEADER)
        worksheet.write_merge(4, 4, 1, 1, company.street.upper() if company.street else '')

        invoice_search_ids = self.env['account.invoice'].search([('date_invoice', '>=', self.start_date), ('date_invoice', '<=', self.end_date), ('type', '=', 'out_invoice'), ('state', 'not in', ['draft'])])
        refund_invoice_search_ids = self.env['account.invoice'].search([('date_invoice', '>=', self.start_date), ('date_invoice', '<=', self.end_date), ('type', '=', 'out_refund'), ('state', 'not in', ['draft'])])

        if not invoice_search_ids and not refund_invoice_search_ids:
            raise UserError(_('Not Available Invoice and Bills Between %s to %s.') % (self.start_date, self.end_date))
        if invoice_search_ids:
            row = 6

            worksheet.write(row, 0, ('NRO DOCUMENTO') or '', for_left_center1) #
            worksheet.write(row, 1, ('F. EMISION') or '', for_left_center1)    # 
            worksheet.write(row, 2, ('F. VENCIMIENTO') or '', for_left_center1) #
            worksheet.write(row, 3, ('TD.') or '', for_left_center1) #
            worksheet.write(row, 4, ('SERIE') or '', for_left_center1) #
            worksheet.write(row, 5, ('NUMERO') or '', for_left_center1) #
            worksheet.write(row, 6, ('TD.') or '', for_left_center1)
            worksheet.write(row, 7, ('NRO DOCUMENTO') or '', for_left_center1)
            worksheet.write(row, 8, ('CLIENTE') or '', for_left_center1)
            worksheet.write(row, 9, ('V. F. EXP') or '', for_left_center1)
            worksheet.write(row, 10, ('BASE IMPONIBLE') or '', for_left_center1)
            worksheet.write(row, 11, ('EXONERADA') or '', for_left_center1)
            worksheet.write(row, 12, ('INAFECTA') or '', for_left_center1)
            worksheet.write(row, 13, ('ISC') or '', for_left_center1)
            worksheet.write(row, 14, ('IGV Y/O IPM') or '', for_left_center1)
            worksheet.write(row, 15, ('OTROS TRIBUTOS') or '', for_left_center1)
            worksheet.write(row, 16, ('TOTAL') or '', for_left_center1)
            worksheet.write(row, 17, ('T. DE CAMBIO') or '', for_left_center1)
            worksheet.write(row, 18, ('F. DOC ORIGEN') or '', for_left_center1)    # 
            worksheet.write(row, 19, ('TD') or '', for_left_center1)    # 
            worksheet.write(row, 20, ('SERIE') or '', for_left_center1)    # 
            worksheet.write(row, 21, ('DOC ORIGEN') or '', for_left_center1)    #


            if not invoice_search_ids:
                raise UserError(_('No Alloy Account Invoice Between %s to %s.') % (self.start_date, self.end_date))
            inv_sub_total = 0
            inv_tax = 0.0
            inv_total = 0.0
            inv_sub_total_exo =0
            val = ''
            val1 = ''

            for invoice in invoice_search_ids:
                doc_types = invoice.partner_id._get_pe_doc_type()
                row = row + 1
                worksheet.write(row, 0, invoice.number or '', for_left_center2)
                worksheet.write(row, 1, invoice.date_invoice or '', for_left_center2)
                worksheet.write(row, 2, invoice.date_due or '', for_left_center2) 
                worksheet.write(row, 3, invoice.journal_id.pe_invoice_code or '', for_left_center2) #######
                worksheet.write(row, 4, invoice.journal_id.code or '', for_left_center2)

                val1 = ''
                if invoice.state != 'cancel':
                    val1 = invoice.number
                    if val1.find('/') > 0:
                        val1 = invoice.number.replace("/","")
                    else:
                        val = val1.split('-')
                        if len(val) > 0:
                            val1 = val[1]
                worksheet.write(row, 5, val1, for_left_center2)
                #worksheet.write(row, 5, val[1] or '', for_left_center2)
                #and dict(doc_types)[invoice.partner_id.doc_type]
                amount_untaxed = invoice.amount_untaxed
                amount_tax = invoice.amount_tax
                amount_total = invoice.amount_total_signed
                partner_name = invoice.partner_id.name
                if invoice.state == 'annul':
                    amount_untaxed = 0
                    amount_tax = 0
                    amount_total = 0
                    partner_name = 'ANULADO'

                if invoice.amount_untaxed > 0:
                    inv_sub_total += invoice.amount_untaxed                
                if invoice.amount_untaxed == 0:
                    inv_sub_total_exo += invoice.amount_untaxed                
                inv_tax += amount_tax
                inv_total += amount_total

                worksheet.write(row, 6, invoice.partner_id.doc_type  or '', for_left_center2)
                worksheet.write(row, 7, invoice.partner_id.doc_number or '', for_left_center2)
                worksheet.write(row, 8, partner_name or '', for_left_center2)
                worksheet.write(row, 9, ' ', for_left_center2)
                worksheet.write(row, 10, amount_untaxed if amount_untaxed > 0 else '', for_left_center3)
                worksheet.write(row, 11, amount_untaxed if amount_untaxed == 0 else '', for_left_center3)
                worksheet.write(row, 12, '0.0', for_left_center3)
                worksheet.write(row, 13, '0.0', for_left_center3)
                worksheet.write(row, 14, amount_tax or  '', for_left_center3)

                worksheet.write(row, 15, '0.0', for_left_center3)
                worksheet.write(row, 16, amount_total or '0.0', for_left_center3)
                worksheet.write(row, 17, invoice.currency_id.rate or '', for_left_center3)
                worksheet.write(row, 18, invoice.refund_invoice_id.date_invoice or '', for_left_center2)
                worksheet.write(row, 19, invoice.refund_invoice_id.journal_id.pe_invoice_code or '', for_left_center2)
                worksheet.write(row, 20, invoice.refund_invoice_id.journal_id.code or '', for_left_center2)
                worksheet.write(row, 21, ' ', for_left_center2)

            row = row + 1
            worksheet.write(row, 10, _('SUB TOTAL'), for_left_center1)
            worksheet.write(row, 11, _('SUB TOTAL'), for_left_center1)
            worksheet.write(row, 14, _('TAX'), for_left_center1)
            worksheet.write(row, 16, _('TOTAL'), for_left_center1)

            row = row + 1
            worksheet.write(row, 10, inv_sub_total or '0.0', for_left_center3)
            worksheet.write(row, 11, inv_sub_total_exo or '0.0', for_left_center3)
            worksheet.write(row, 14, inv_tax or '0.0', for_left_center3)
            worksheet.write(row, 16, inv_total or '0.0', for_left_center3)  

        if refund_invoice_search_ids:
            row = row + 2
            vendor_details_title = ('Facturas de cliente Rectificativas')
            worksheet.write_merge(row, row + 0, 0, 18, vendor_details_title, GREEN_TABLE_HEADER)

            if refund_invoice_search_ids:
                row = row + 2 

                worksheet.write(row, 0, ('NRO DOCUMENTO') or '', for_left_center1) #
                worksheet.write(row, 1, ('F. EMISION') or '', for_left_center1)    # 
                worksheet.write(row, 2, ('F. VENCIMIENTO') or '', for_left_center1) #
                worksheet.write(row, 3, ('TD.') or '', for_left_center1) #
                worksheet.write(row, 4, ('SERIE') or '', for_left_center1) #
                worksheet.write(row, 5, ('NUMERO') or '', for_left_center1) #
                worksheet.write(row, 6, ('TD.') or '', for_left_center1)
                worksheet.write(row, 7, ('NRO DOCUMENTO') or '', for_left_center1)
                worksheet.write(row, 8, ('CLIENTE') or '', for_left_center1)
                worksheet.write(row, 9, ('V. F. EXP') or '', for_left_center1)
                worksheet.write(row, 10, ('BASE IMPONIBLE') or '', for_left_center1)
                worksheet.write(row, 11, ('EXONERADA') or '', for_left_center1)
                worksheet.write(row, 12, ('INAFECTA') or '', for_left_center1)
                worksheet.write(row, 13, ('ISC') or '', for_left_center1)
                worksheet.write(row, 14, ('IGV Y/O IPM') or '', for_left_center1)
                worksheet.write(row, 15, ('OTROS TRIBUTOS') or '', for_left_center1)
                worksheet.write(row, 16, ('TOTAL') or '', for_left_center1)
                worksheet.write(row, 17, ('T. DE CAMBIO') or '', for_left_center1)
                worksheet.write(row, 18, ('F. DOC ORIGEN') or '', for_left_center1)    # 
                worksheet.write(row, 19, ('TD') or '', for_left_center1)    # 
                worksheet.write(row, 20, ('SERIE') or '', for_left_center1)    # 
                worksheet.write(row, 21, ('DOC ORIGEN') or '', for_left_center1)    #


            if not refund_invoice_search_ids:
                raise UserError(_('No Alloy Vendor Bills Between %s to %s.') % (self.start_date, self.end_date))

            refund_inv_sub_total = 0
            refund_inv_tax = 0.0
            refund_inv_total = 0.0
            refund_inv_inv_sub_total_exo =0
            val = ''
            val1 = ''
            val0 = ''
            val2 = '' 
            for refund_inv in refund_invoice_search_ids:
                row = row + 1
                worksheet.write(row, 0, refund_inv.number or '', for_left_center2)
                worksheet.write(row, 1, refund_inv.date_invoice or '', for_left_center2)
                worksheet.write(row, 2, refund_inv.date_due or '', for_left_center2) 
                worksheet.write(row, 3, refund_inv.journal_id.pe_invoice_code or '', for_left_center2) #######
                worksheet.write(row, 4, refund_inv.journal_id.code or '', for_left_center2)
                if refund_inv.number:
                    val1= refund_inv.number
                    val= val1.split('-')
                    worksheet.write(row, 5, val[1] or '', for_left_center2)                

                amount_untaxed = refund_inv.amount_untaxed_signed

                amount_tax = refund_inv.amount_tax*-1
                amount_total = refund_inv.amount_total_signed
                partner_name = refund_inv.partner_id.name

                if invoice.state == 'annul':
                    amount_untaxed = 0
                    amount_tax = 0
                    amount_total = 0
                    partner_name = 'ANULADO'

                if refund_inv.amount_untaxed_signed != 0:
                    refund_inv_sub_total += amount_untaxed               
                if refund_inv.amount_untaxed == 0:
                    refund_inv_inv_sub_total_exo += amount_untaxed       
                refund_inv_tax += amount_tax
                refund_inv_total += amount_total

                worksheet.write(row, 6, refund_inv.partner_id.doc_type or '', for_left_center2)
                worksheet.write(row, 7, refund_inv.partner_id.doc_number or '', for_left_center2)
                worksheet.write(row, 8, partner_name or '', for_left_center2)
                worksheet.write(row, 9, '0.0', for_left_center2)
                worksheet.write(row, 10, amount_untaxed if refund_inv.amount_untaxed > 0 else '', for_left_center3)

                
                worksheet.write(row, 11, amount_untaxed if refund_inv.amount_untaxed_signed == 0 else '', for_left_center3)
                worksheet.write(row, 12, '0.0', for_left_center3)
                worksheet.write(row, 13, '0.0', for_left_center3)                             
                worksheet.write(row, 14, refund_inv_tax or  '', for_left_center3)

                worksheet.write(row, 15, '0.0', for_left_center3)
                worksheet.write(row, 16, refund_inv_total or '0.0', for_left_center3)

                worksheet.write(row, 17, refund_inv.currency_id.rate or '', for_left_center3)
                worksheet.write(row, 18, refund_inv.refund_invoice_id.date_invoice or '', for_left_center2)
                worksheet.write(row, 19, refund_inv.refund_invoice_id.journal_id.pe_invoice_code or '', for_left_center2)
                worksheet.write(row, 20, refund_inv.refund_invoice_id.journal_id.code or '', for_left_center2)
                if refund_inv.origin:
                    val0= refund_inv.origin
                    val1= val0.split('-')
                    worksheet.write(row, 21, val1[1] or '', for_left_center2)                 
                #orksheet.write(row, 18, refund_inv.origin or '', for_left_center2)

            row = row + 1
            worksheet.write(row, 10, _('SUB TOTAL'), for_left_center1)
            worksheet.write(row, 11, _('SUB TOTAL'), for_left_center1)
            worksheet.write(row, 14, _('TAX'), for_left_center1)
            worksheet.write(row, 16, _('TOTAL'), for_left_center1)          

            row = row + 1
            worksheet.write(row, 10, refund_inv_sub_total or '0.0', for_left_center3)
            worksheet.write(row, 11, refund_inv_inv_sub_total_exo or '0.0', for_left_center3)
            worksheet.write(row, 14, refund_inv_tax or '0.0', for_left_center3)
            worksheet.write(row, 16, refund_inv_total or '0.0', for_left_center3)               

        fp = BytesIO()
        workbook.save(fp)
        account_invoice_detail_excel_id = self.env['account.invoice.excel.extended'].create({'excel_file': base64.encodestring(fp.getvalue()), 'file_name': filename})
        fp.close()

        return {
            'type': 'ir.actions.act_url',
            'url': '/web/binary/download_xls_report/%s' % (account_invoice_detail_excel_id.id),
            'target': 'new',
        }

    @api.multi
    def account_invoice_excel_reportss(self):
        dt_start = datetime.datetime.strptime(self.start_date, DEFAULT_SERVER_DATE_FORMAT)
        date_start = dt_start.strftime('%d-%m-%Y')
        dt_end = datetime.datetime.strptime(self.end_date, DEFAULT_SERVER_DATE_FORMAT)
        date_end = dt_end.strftime('%d-%m-%Y')
        filename = 'Invoice Report From ' + str(date_start) + ' To ' + str(date_end) + '.xls'
        workbook = xlwt.Workbook(encoding="UTF-8")

        worksheet = workbook.add_sheet('Account Invoice Details')
        font = xlwt.Font()
        font.bold = True

        GREEN_TABLE_HEADER = xlwt.easyxf(
                 'font: bold 1, name Tahoma, height 250;'
                 'align: vertical center, wrap on;'
                 #'borders: top medium, bottom medium, left medium, right medium;'
                 )
        style = xlwt.easyxf('font:height 400, bold True, name Arial; align: horiz center, vert center;borders: top medium,right medium,bottom medium,left medium')
        for_left_center1 = xlwt.easyxf("font: bold 1, color black; align: horiz center; borders: top medium, bottom medium, left medium, right medium;pattern: pattern solid, pattern_fore_colour lime, pattern_back_colour lime;")
        for_left_center2 = xlwt.easyxf("font: color black; align: horiz left; borders: top medium, bottom medium, left medium, right medium;")
        for_left_center3 = xlwt.easyxf("font: color black; align: horiz right; borders: top medium, bottom medium, left medium, right medium;")
        for_left_center4 = xlwt.easyxf("font: color black; align: horiz center; borders: top medium, bottom medium, left medium, right medium;")
        alignment = xlwt.Alignment()
        alignment.horz = xlwt.Alignment.HORZ_RIGHT
        style = xlwt.easyxf('align: wrap yes')
        style.num_format_str = '0.00'

        worksheet.row(0).height = 320
        worksheet.col(0).width = 4000
        worksheet.col(1).width = 4000
        worksheet.col(2).width = 4000
        worksheet.col(3).width = 3000
        worksheet.col(4).width = 3000
        worksheet.col(5).width = 4000
        worksheet.col(6).width = 2000
        worksheet.col(7).width = 4000
        worksheet.col(8).width = 7000
        worksheet.col(9).width = 4000
        worksheet.col(10).width = 4000
        worksheet.col(11).width = 4000
        worksheet.col(12).width = 4000
        worksheet.col(13).width = 4000
        worksheet.col(14).width = 4000
        worksheet.col(15).width = 4000
        worksheet.col(16).width = 3000
        worksheet.col(17).width = 3000
        worksheet.col(18).width = 4000
        worksheet.col(19).width = 4000        
        worksheet.col(20).width = 4000        
        worksheet.col(21).width = 4000        
        worksheet.col(22).width = 4000   
        worksheet.col(23).width = 4000   

        borders = xlwt.Borders()
        borders.bottom = xlwt.Borders.MEDIUM
        border_style = xlwt.XFStyle()
        border_style.borders = borders

        company = self.env['res.company'].search([])[0].partner_id          
        invoice_details_title = ('Facturas de proveedor')
        worksheet.write_merge(0, 0, 0, 20, invoice_details_title, GREEN_TABLE_HEADER)
        worksheet.write_merge(1, 1, 0, 0, "Periodo:".upper(), GREEN_TABLE_HEADER)    
        worksheet.write_merge(1, 1, 1, 1, self.start_date)
        worksheet.write_merge(1, 1, 2, 2, self.end_date)
        worksheet.write_merge(2, 2, 0, 0, "RUC:", GREEN_TABLE_HEADER)
        worksheet.write_merge(2, 2, 1, 1, company.company_id.partner_id.doc_number)
        worksheet.write_merge(3, 3, 0, 0, "Empresa:".upper(), GREEN_TABLE_HEADER)
        worksheet.write_merge(3, 3, 1, 1, company.name.upper(), )
        worksheet.write_merge(4, 4, 0, 0, u"Dirección:".upper(), GREEN_TABLE_HEADER)
        worksheet.write_merge(4, 4, 1, 1, company.street.upper() if company.street else '')


        bill_search_ids = self.env['account.invoice'].search([('date_invoice', '>=', self.start_date), ('date_invoice', '<=', self.end_date), ('type', '=', 'in_invoice')])
        refund_bill_search_ids = self.env['account.invoice'].search([('date_invoice', '>=', self.start_date), ('date_invoice', '<=', self.end_date), ('type', '=', 'in_refund')])

        if not bill_search_ids and not refund_bill_search_ids:
            raise UserError(_('Not Available Invoice and Bills Between %s to %s.') % (self.start_date, self.end_date))

        if bill_search_ids:
            row = 6
            vendor_details_title = _('Vendor Bills')
            worksheet.write_merge(row, row + 0, 0, 18, vendor_details_title, GREEN_TABLE_HEADER)

            if bill_search_ids:
                row = row + 1

                worksheet.write(row, 0, ('NRO DOCUMENTO') or '', for_left_center1) #
                worksheet.write(row, 1, ('F. EMISION') or '', for_left_center1)    # 
                worksheet.write(row, 2, ('F. VENCIMIENTO') or '', for_left_center1) #
                worksheet.write(row, 3, ('TD.') or '', for_left_center1) #
                worksheet.write(row, 4, ('SERIE') or '', for_left_center1) #
                worksheet.write(row, 5, ('NUMERO') or '', for_left_center1) #
                worksheet.write(row, 6, ('TD.') or '', for_left_center1)
                worksheet.write(row, 7, ('NRO DOCUMENTO') or '', for_left_center1)
                worksheet.write(row, 8, ('CLIENTE') or '', for_left_center1)
                worksheet.write(row, 9, ('V. F. EXP') or '', for_left_center1)
                worksheet.write(row, 10, ('BASE IMPONIBLE') or '', for_left_center1)
                worksheet.write(row, 11, ('EXONERADA') or '', for_left_center1)
                worksheet.write(row, 12, ('INAFECTA') or '', for_left_center1)
                worksheet.write(row, 13, ('ISC') or '', for_left_center1)
                worksheet.write(row, 14, ('IGV Y/O IPM') or '', for_left_center1)
                worksheet.write(row, 15, ('OTROS TRIBUTOS') or '', for_left_center1)
                worksheet.write(row, 16, ('TOTAL') or '', for_left_center1)
                worksheet.write(row, 17, ('T. DE CAMBIO') or '', for_left_center1)
                worksheet.write(row, 18, ('F. DOC ORIGEN') or '', for_left_center1)    # 
                worksheet.write(row, 19, ('TD') or '', for_left_center1)    # 
                worksheet.write(row, 20, ('SERIE') or '', for_left_center1)    # 
                worksheet.write(row, 21, ('DOC ORIGEN') or '', for_left_center1)    #
                worksheet.write(row, 22, ('REFERENCIA') or '', for_left_center1)    #

            if not bill_search_ids:
                raise UserError(_('No Alloy Vendor Bills Between %s to %s.') % (self.start_date, self.end_date))

            bill_sub_total = 0
            bill_tax = 0.0
            bill_total = 0.0
            bill_inv_sub_total_exo =0
            val = ''
            val1 = ''

            for bills in bill_search_ids:
                row = row + 1
                worksheet.write(row, 0, bills.number or '', for_left_center2)
                worksheet.write(row, 1, bills.date_invoice or '', for_left_center2)
                worksheet.write(row, 2, bills.date_due or '', for_left_center2) 
                worksheet.write(row, 3, bills.journal_id.pe_invoice_code or '', for_left_center2) #######
                worksheet.write(row, 4, bills.journal_id.code or '', for_left_center2)
                if bills.number:
                    val1= bills.number
                    val= val1.split('-')
                    worksheet.write(row, 5, val[1] or '', for_left_center2)
                worksheet.write(row, 6, bills.partner_id.doc_type or '', for_left_center2)
                worksheet.write(row, 7, bills.partner_id.doc_number or '', for_left_center2)
                worksheet.write(row, 8, bills.partner_id.name or '', for_left_center2)
                worksheet.write(row, 9, '0.0', for_left_center3)                
                worksheet.write(row, 10, bills.amount_untaxed if bills.amount_untaxed > 0 else '',for_left_center3)
                if bills.amount_untaxed > 0:
                    bill_sub_total += bills.amount_untaxed                
                worksheet.write(row, 11, bills.amount_untaxed if bills.amount_untaxed == 0 else '',for_left_center3)
                if bills.amount_untaxed == 0:
                    bill_inv_sub_total_exo += bills.amount_untaxed                
                worksheet.write(row, 12, '0.0', for_left_center3)
                worksheet.write(row, 13, '0.0', for_left_center3)                                
                worksheet.write(row, 14, bills.amount_tax or  '', for_left_center3)
                bill_tax += bills.amount_tax                
                worksheet.write(row, 15, '0.0', for_left_center2)
                worksheet.write(row, 16, bills.amount_total or '0.0', for_left_center3)
                bill_total += bills.amount_total
                worksheet.write(row, 17, bills.currency_id.rate or '', for_left_center3)
                worksheet.write(row, 18, bills.refund_invoice_id.date_invoice or '', for_left_center2)
                worksheet.write(row, 19, bills.refund_invoice_id.journal_id.pe_invoice_code or '', for_left_center2)
                worksheet.write(row, 20, bills.refund_invoice_id.journal_id.code or '', for_left_center2)          
                worksheet.write(row, 21, ' ', for_left_center2)
                worksheet.write(row, 22, bills.refund_invoice_id.name or '', for_left_center2)          

            row = row + 1
            worksheet.write(row, 10, _('SUB TOTAL'), for_left_center1)
            worksheet.write(row, 11, _('SUB TOTAL'), for_left_center1)
            worksheet.write(row, 14, _('TAX'), for_left_center1)
            worksheet.write(row, 16, _('TOTAL'), for_left_center1)          

            row = row + 1
            worksheet.write(row, 10, bill_sub_total or '0.0', for_left_center3)
            worksheet.write(row, 11, bill_inv_sub_total_exo or '0.0', for_left_center3)
            worksheet.write(row, 14, bill_tax or '0.0', for_left_center3)
            worksheet.write(row, 16, bill_total or '0.0', for_left_center3)               

        if refund_bill_search_ids:
            row = row + 1
            vendor_details_title = ('Facturas de proveedor Rectificativas')
            worksheet.write_merge(row, row + 0, 0, 18, vendor_details_title, GREEN_TABLE_HEADER)

            if refund_bill_search_ids:
                row = row + 1
                worksheet.write(row, 0, ('NRO DOCUMENTO') or '', for_left_center1) #
                worksheet.write(row, 1, ('F. EMISION') or '', for_left_center1)    # 
                worksheet.write(row, 2, ('F. VENCIMIENTO') or '', for_left_center1) #
                worksheet.write(row, 3, ('TD.') or '', for_left_center1) #
                worksheet.write(row, 4, ('SERIE') or '', for_left_center1) #
                worksheet.write(row, 5, ('NUMERO') or '', for_left_center1) #
                worksheet.write(row, 6, ('TD.') or '', for_left_center1)
                worksheet.write(row, 7, ('NRO DOCUMENTO') or '', for_left_center1)
                worksheet.write(row, 8, ('CLIENTE') or '', for_left_center1)
                worksheet.write(row, 9, ('V. F. EXP') or '', for_left_center1)
                worksheet.write(row, 10, ('BASE IMPONIBLE') or '', for_left_center1)
                worksheet.write(row, 11, ('EXONERADA') or '', for_left_center1)
                worksheet.write(row, 12, ('INAFECTA') or '', for_left_center1)
                worksheet.write(row, 13, ('ISC') or '', for_left_center1)
                worksheet.write(row, 14, ('IGV Y/O IPM') or '', for_left_center1)
                worksheet.write(row, 15, ('OTROS TRIBUTOS') or '', for_left_center1)
                worksheet.write(row, 16, ('TOTAL') or '', for_left_center1)
                worksheet.write(row, 17, ('T. DE CAMBIO') or '', for_left_center1)
                worksheet.write(row, 18, ('F. DOC ORIGEN') or '', for_left_center1)    # 
                worksheet.write(row, 19, ('TD') or '', for_left_center1)    # 
                worksheet.write(row, 20, ('SERIE') or '', for_left_center1)    # 
                worksheet.write(row, 21, ('DOC ORIGEN') or '', for_left_center1)    #
                worksheet.write(row, 22, ('REFERENCIA') or '', for_left_center1)    #


            if not refund_bill_search_ids:
                raise UserError(_('No Alloy Vendor Bills Between %s to %s.') % (self.start_date, self.end_date))

            refund_bill_sub_total = 0
            refund_bill_tax = 0.0
            refund_bill_total = 0.0
            refund_bill_inv_sub_total_exo =0
            val = ''
            val1 = ''
            val0 = ''
            val2 = ''            

            for refund_bills in refund_bill_search_ids:
                row = row + 1
                worksheet.write(row, 0, refund_bills.number or '', for_left_center2)
                worksheet.write(row, 1, refund_bills.date_invoice or '', for_left_center2)
                worksheet.write(row, 2, refund_bills.date_due or '', for_left_center2) 
                worksheet.write(row, 3, refund_bills.journal_id.pe_invoice_code or '', for_left_center2) #######
                worksheet.write(row, 4, refund_bills.journal_id.code or '', for_left_center2)
                worksheet.write(row, 5, refund_bills.number or '', for_left_center2)
                if refund_bills.origin:
                    val1= refund_bills.origin
                    val= val1.split('-')
                    worksheet.write(row, 5, val[1] or '', for_left_center2) 
                worksheet.write(row, 6, refund_bills.partner_id.doc_type or '', for_left_center2)
                worksheet.write(row, 7, refund_bills.partner_id.doc_number or '', for_left_center2)
                worksheet.write(row, 8, refund_bills.partner_id.name or '', for_left_center2)
                worksheet.write(row, 9, '0.0', for_left_center3)                                
                worksheet.write(row, 10, refund_bills.amount_untaxed if refund_bills.amount_untaxed > 0 else '', for_left_center3)
                if refund_bills.amount_untaxed > 0:
                    refund_bill_sub_total += refund_bills.amount_untaxed                
                worksheet.write(row, 11, refund_bills.amount_untaxed if refund_bills.amount_untaxed == 0 else '', for_left_center3)
                if refund_bills.amount_untaxed == 0:
                    refund_bill_inv_sub_total_exo += refund_bills.amount_untaxed  

                worksheet.write(row, 12, '0', for_left_center3)                                                                 
                worksheet.write(row, 13, '0', for_left_center3)                                                                 
                worksheet.write(row, 14, refund_bills.amount_tax or  '', for_left_center3)
                refund_bill_tax += refund_bills.amount_tax                
                worksheet.write(row, 15, '0.0', for_left_center3)
                worksheet.write(row, 16, refund_bills.amount_total or '0.0', for_left_center3)
                refund_bill_total += refund_bills.amount_total
                worksheet.write(row, 17, refund_bills.currency_id.rate or '', for_left_center3)
                worksheet.write(row, 18, refund_bills.refund_invoice_id.date_invoice or '', for_left_center2)
                worksheet.write(row, 19, refund_bills.refund_invoice_id.journal_id.pe_invoice_code or '', for_left_center2)
                worksheet.write(row, 20, refund_bills.refund_invoice_id.journal_id.code or '', for_left_center2)
                if refund_bills.origin:
                    val0= refund_bills.origin
                    val2= val0.split('-')
                    worksheet.write(row, 21, val2[1] or '', for_left_center2)                
                worksheet.write(row, 22, refund_bills.refund_invoice_id.name or '', for_left_center2)
                #worksheet.write(row, 18, refund_bills.origin or '', for_left_center2)

            row = row + 1
            worksheet.write(row, 10, _('SUB TOTAL'), for_left_center1)
            worksheet.write(row, 12, _('SUB TOTAL'), for_left_center1)
            worksheet.write(row, 14, _('TAX'), for_left_center1)
            worksheet.write(row, 16, _('TOTAL'), for_left_center1)

            row = row + 1
            worksheet.write(row, 10, refund_bill_sub_total or '0.0', for_left_center3)
            worksheet.write(row, 11, refund_bill_inv_sub_total_exo or '0.0', for_left_center3)
            worksheet.write(row, 14, refund_bill_tax or '0.0', for_left_center3)
            worksheet.write(row, 16, refund_bill_total or '0.0', for_left_center3)               


        fp = BytesIO()
        workbook.save(fp)
        account_invoice_detail_excel_id = self.env['account.invoice.excel.extended'].create({'excel_file': base64.encodestring(fp.getvalue()), 'file_name': filename})
        fp.close()

        return {
            'type': 'ir.actions.act_url',
            'url': '/web/binary/download_xls_report/%s' % (account_invoice_detail_excel_id.id),
            'target': 'new',
        }


class AccountInvoiceExcelExtended(models.Model):
    _name = 'account.invoice.excel.extended'

    excel_file = fields.Binary('Download Report :- ')
    file_name = fields.Char('Excel File', size=64)
