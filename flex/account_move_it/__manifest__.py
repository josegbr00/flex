# -*- encoding: utf-8 -*-
{
	'name': 'Account Move IT',
	'category': 'account',
	'author': 'FLEXXOONE-COMPATIBLE-BO',
	'depends': ['account','odoope_einvoice_base','account_rendicion_base_it'],
	'version': '1.0.0',
	'description':"""
		Agrega los Campos al Asiento Contable
	""",
	'auto_install': False,
	'demo': [],
	'data':	['views/account_move_view.xml'],
	'installable': True
}
