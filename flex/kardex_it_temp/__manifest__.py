# -*- encoding: utf-8 -*-
{
    'name': 'Kardex',
    'version': '1.0',
    'author': 'FLEXXOONE-COMPATIBLE-BO',
    'website': '',
    'category': 'account',
    'depends': ['stock_account','product','stock','mrp','purchase_requisition','sale_stock','product_brand'],
    'description': """KARDEX TEMP""",
    'demo': [],
    'data': [
        'views/stock_picking_views.xml',
    ],
    'auto_install': False,
    'installable': True
}
