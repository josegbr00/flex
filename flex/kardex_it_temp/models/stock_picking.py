# -*- coding: utf-8 -*-

from collections import namedtuple
import json
import time

from odoo import api, fields, models, _, exceptions
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from odoo.tools.float_utils import float_compare
#from odoo.addons.procurement.models import procurement
from odoo.exceptions import UserError


class PickingType(models.Model):
    _inherit = "stock.picking"
    fecha_kardex = fields.Date(string='Fecha kardex', readonly=False)
    invoice_id = fields.Many2one('account.invoice', 'Factura')
    state_invoice = fields.Char(u'Estado de factura', compute='calc_state_invoice')
    es_fecha_kardex = fields.Boolean('Usar Fecha kardex', default=True)
    po_id = fields.Many2one('stock.picking', 'Orden de pedido')
    marca = fields.Char('Marca', size=12)
    placa = fields.Char('Placa', size=12)
    nro_const = fields.Char('Numero de constancia de inscripcion', size=12)
    licencia = fields.Char('Licencia de Conducir N°(5)', size=12)
    nombre = fields.Char('Nombre')
    ruc = fields.Char('Ruc', size=100)
    tipo = fields.Char('Tipo', size=12)
    nro_comp = fields.Char('Numero de comprobante', size=12)
    nro_guia = fields.Char('Numero de guia', size=12)
    fecha_traslado = fields.Datetime(string='Fecha de traslado')
    punto_partida = fields.Char('Punto de partida', size=100)
    punto_llegada = fields.Char('Punto de llegada', size=100)

    # @api.model
    # def create(self, vals):
    #     t = super(PickingType, self).create(vals)
    #     if t.picking_type_id.warehouse_id.id and t.picking_type_id.warehouse_id.partner_id.id and t.picking_type_id.warehouse_id.partner_id.street:
    #         t.punto_partida = t.picking_type_id.warehouse_id.partner_id.street
    #     if t.partner_id.id and t.partner_id.street:
    #         t.punto_llegada = t.partner_id.street
    #     return t

    @api.one
    def calc_state_invoice(self):
        if self.invoice_id:
            dic_state = {'draft': 'Borrador', 'proforma': 'Pro-forma', 'proforma2': 'Pro-forma', 'open': 'Abierto',
                         'paid': 'Pagado', 'cancel': 'Cancelado'}
            self.state_invoice = dic_state[self.invoice_id.state]
        else:
            self.state_invoice = ''

    @api.one
    def write(self, vals):
        # if 'partner_id' in vals:
        #     if vals['partner_id']:
        #         p = self.env['res.partner'].browse(vals['partner_id'])
        #         vals['punto_llegada'] = p.street if p.street else False
        #     else:
        #         vals['punto_llegada'] = False

        if 'picking_type_id' in vals:
            p = self.env['stock.picking.type'].browse(vals['picking_type_id'])
            if p.warehouse_id.id and p.warehouse_id.partner_id.id:
                vals['punto_partida'] = p.warehouse_id.partner_id.street if p.warehouse_id.partner_id.street else False

        t = super(PickingType, self).write(vals)
        return t
