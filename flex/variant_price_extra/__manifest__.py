# -*- coding: utf-8 -*-
#################################################################################
# Author      : Webkul Software Pvt. Ltd. (<https://webkul.com/>)
# Copyright(c): 2015-Present Webkul Software Pvt. Ltd.
# All Rights Reserved.
#
#
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#
# You should have received a copy of the License along with this program.
# If not, see <https://store.webkul.com/license.html/>
#################################################################################
{
  "name"                 :  "Product Variant Extra Price",
  "summary"              :  "This module allows you to manually apply additional extra prices for Product's variants.",
  "category"             :  "Uncategorized",
  "version"              :  "1.0",
  "sequence"             :  1,
  "author"               :  "Webkul Software Pvt. Ltd.",
  "website"              :  "https://store.webkul.com/Odoo-Product-Variant-Extra-Price.html",
  "description"          :  "http://webkul.com/blog/product-variant-extra-price-different-weight/",
  "live_test_url"        :  "http://odoodemo.webkul.com/?module=variant_price_extra&version=10.0",
  "depends"              :  ['product'],
  "data"                 :  ['views/product_inherit_view.xml'],
  "images"               :  ['static/description/Banner.png'],
  "application"          :  True,
  "installable"          :  True,
  "auto_install"         :  False,
  "price"                :  20,
  "currency"             :  "EUR",
  "pre_init_hook"        :  "pre_init_check",
} 